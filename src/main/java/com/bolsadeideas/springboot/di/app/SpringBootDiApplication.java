package com.bolsadeideas.springboot.di.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class SpringBootDiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDiApplication.class, args);
	}

}
